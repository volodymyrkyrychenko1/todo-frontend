const input = document.getElementById("input");
const addBtn = document.getElementById("add");
const clearBtn = document.getElementById("clear");
const list = document.getElementById("list");

let todos = [];

const callAPI = async (method, route, obj) => {
  const server = "http://127.0.0.1:8000";
  const options = {
    method,
    body: JSON.stringify(obj),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  };

  const url = `${server}${route}`;
  const response = await fetch(url, options);

  try {
    return await response.json();
  } catch (error) {
    console.log("Error: ", error);
  }
};

const requests = {
  getTodo: (todo) => {
    return callAPI("POST", "/todo", todo);
  },
  getTodos: async () => {
    return callAPI("GET", "/todos");
  },
  createTodo: async (data) => {
    return await callAPI("POST", "/todo", data);
  },
  deleteTodo: async (todo) => {
    return callAPI("DELETE", "/todo", todo);
  },
  deleteAllTodo: () => {
    return callAPI("DELETE", "/todo", { all: true });
  },
  editTodo: (todo) => {
    return callAPI("POST", "/todo", todo);
  },
};

const notifyText = {
  MIN_LENGTH: "Denied! Min entry length must be more than 3 characters",
  MAX_LENGTH: "Denied! Max length must be shorter than 40 characters",
};

const countTodoItems = (arr) => {
  counter.textContent = arr.length;
};

function checkForOverflow() {
  const collection = document.querySelectorAll("li");
  if (collection.length > 3) {
    list.style.overflowY = "scroll";
  } else if (collection.length < 3) {
    list.style.overflowY = "hidden";
  }
  countTodoItems(collection);
}

const setAttributes = (el, attrs) => {
  for (const key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
};

const showNotification = (text) => {
  const el = document.createElement("div");
  const txt = document.createTextNode(notifyText[text]);

  el.setAttribute("class", "notification");
  input.classList.toggle("input-error");

  setTimeout(() => {
    input.classList.toggle("input-error");
    el.classList.toggle("hide");
  }, 3000);

  el.appendChild(txt);
  document.body.appendChild(el);
};

const validate = ({description}) => {
  return Object.keys(notifyText).find(key => notifyText[key] === description)

};

const getEditTodo = ({ text, id }) => {
  let todoIndex = todos.findIndex((todo) => todo.id == id);
  todos[todoIndex].text = text;
  renderTodoList(todos);
};

const save = async (e) => {
  const todo = e.target.closest("li");
  const buttons = todo.querySelectorAll(".btn");
  const text = todo.querySelector("span").innerHTML;
  const id = todo.id;

  getEditTodo(await requests.editTodo({ text, id }));
  todo.classList.toggle("active-todo");
  buttons.forEach((btn) => btn.classList.toggle("hide"));
};

const updateTodo = async (e) => {
  const todo = e.currentTarget;
  const span = todo.querySelector("span");
  const buttons = todo.querySelectorAll(".btn");

  if (todo.classList.contains("active-todo")) return;

  todo.classList.toggle("active-todo");
  span.contentEditable = true;
  span.focus();
  buttons.forEach((btn) => btn.classList.toggle("hide"));
  buttons[0].addEventListener("click", save);
};

const addTodoToList = async (todo) => {
  if (!todo) return;
  const listItem = document.createElement("li");
  const text = document.createElement("span");
  const removeBtn = document.createElement("button");
  const saveBtn = document.createElement("button");
  const btnContainer = document.createElement("div");
  const firstChild = list.firstChild;

  localStorage.setItem("todos", JSON.stringify(todos));

  setAttributes(listItem, { class: "list-item", id: `${todo.id}` });
  setAttributes(saveBtn, {
    type: "button",
    "data-item": "save",
    class: "btn btn-save hide",
  });
  setAttributes(removeBtn, {
    type: "button",
    "data-item": "remove",
    class: "btn btn-purple btn-remove ",
  });
  saveBtn.innerHTML = "save";

  const newText = document.createTextNode(todo.text);
  text.appendChild(newText);
  listItem.appendChild(text);
  btnContainer.appendChild(saveBtn);
  btnContainer.appendChild(removeBtn);
  listItem.appendChild(btnContainer);
  listItem.addEventListener("dblclick", updateTodo);
  list.insertBefore(listItem, firstChild);

  checkForOverflow();
  input.value = "";
};

const clearAllTodos = () => {
  const todos = document.querySelectorAll("li");
  todos.forEach((todo) => todo.remove());
  localStorage.clear();
  checkForOverflow();
};

const deleteTodoFromList = (id) => {
  document.getElementById(id).remove();
  console.log(todos);
  todos = todos.filter((todo) => todo.id !== id);
  renderTodoList(todos);

  checkForOverflow();
};

const renderTodoList = (arr) => {
  list.innerHTML = "";
  arr.forEach((todo) => addTodoToList(todo));
  input.value = "";
  countTodoItems(arr);
  checkForOverflow(arr);
};

window.addEventListener("load", async () => {
  let saved = localStorage.getItem("todos");
  if (saved) {
    todos = JSON.parse(localStorage.getItem("todos"));
    renderTodoList(todos);
    let todosFromBackend = await requests.getTodos();
    if (todos.length !== todosFromBackend.length) {
      todos = todosFromBackend;
      renderTodoList(todos);
    }
  } else {
    renderTodoList(todos);
  }
});

clearBtn.addEventListener("click", async () => {
  clearAllTodos(await requests.deleteAllTodo());
});

list.addEventListener("click", async (e) => {
  const isButton = e.target.classList.contains("btn-remove");
  e.preventDefault();

  if (!isButton) return;
  const removeTodo = { id: e.target.closest("li").id };
  deleteTodoFromList(await requests.deleteTodo(removeTodo));
});

const getResponseCreateTodo = async (todo) => {
  const reason = validate(todo);
  if (reason) {
    showNotification(reason);
    return;
  }
  todos.push(todo);
  renderTodoList(todos);
};

addBtn.addEventListener("click", async () => {
  const text = input.value;
  try {
    getResponseCreateTodo(await requests.createTodo({ text }));
  } catch (error) {
    console.log(error);
  }
});
